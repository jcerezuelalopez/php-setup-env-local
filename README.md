## Before starting
Before starting it is necessary to have installed and configured:

- Ansible
- Git (Optional)

Install git
```
sudo apt update
sudo apt install git
```

Install Ansible
```
sudo apt update
sudo apt install ansible
```

## Clone repository (optional)

Open terminal, ``Ctrl+Alt+t``,

```
git clone https://gitlab.com/jcerezuelalopez/php-setup-env-local.git
cd php-setup-env-local
```
## Run playbook
An inside the project folder
``
ansible-playbook -i inventory -K main.yml
``
## Dependencies to install
- Composer
- Mysql server
- Mysql client
- Python3-mysqldb
- Libmysqlclient-dev
- Git
- Vim
- Software-properties-common
- PHP 5.6
- PHP 7.4
- PHP 8.1
- PHP5.6-mysql
- PHP5.6-curl
- PHP5.6-json
- PHP5.6-cgi
- PHP5.6-xsl
- PHP5.6-gd
- PHP5.6-mbstring
- PHP5.6-zip
- PHP5.6-xmlrpc
- PHP5.6-soap
- PHP5.6-intl
- PHP7.4-mysql
- PHP7.4-curl
- PHP7.4-json
- PHP7.4-cgi
- PHP7.4-xsl
- PHP7.4-gd
- PHP7.4-mbstring
- PHP7.4-zip
- PHP7.4-xmlrpc
- PHP7.4-soap
- PHP7.4-intl
- Libapache2-mod-php
- PHP8.1-cli
- PHP8.1-common
- PHP8.1-mysql
- PHP8.1-zip
- PHP8.1-gd
- PHP8.1-mbstring
- PHP8.1-curl
- PHP8.1-xml
- PHP8.1-bcmath
- Apache2
- DBeaver-ce
## Miscellany
### Apache2 Rewrite Module
PThe loading of the Apache rewrite module may fail, in which case execute the following commands in the terminal.
```
sudo a2enmod rewrite
sudo service apache2 restart
```

### Switch PHP versions
In the example below we are going to change the version of PHP 8.1 to 7.4
```
# Choose the version of PHP we want to use
sudo update-alternatives --config php

# Disable the Apache module corresponding to the old version of PHP
sudo a2dismod php8.1

# Enable the Apache module corresponding to the new version of PHP
sudo a2dismod php7.4
```


# MySQL Error: : 'Access denied for user 'root'@'localhost'

Open a terminal `ctrl + alt + T`

```
# skip-grant-tables
service mysql stop
mkdir -p /var/run/mysqld
chown mysql:mysql /var/run/mysqld
mysqld_safe --skip-grant-tables --skip-syslog --skip-networking
```

Open a new terminal `ctrl + alt + T`

```
mysql -u root -p
update user set plugin="mysql_native_password" where User='root';
flush privileges;
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
\q
```

Stop mysqld_safe and start mysql

```
# Stop mysqld_safe 
mysqladmin -u root -p shutdown
# 
service mysql start
```
